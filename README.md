# Yelp Review Project
Java Springboot project that uses Yelp API to get reviews of my favorite restaurant.

Restaurant Milwaukee Ale House id: jJwwrFaVJdlwMJjtXsLJmQ

- Review: localhost:8080/{id}/reviews
- Review with vision emotion: localhost:8080/{id}/reviews-emotion
- Detect Face: localhost:8080/detectFaces
