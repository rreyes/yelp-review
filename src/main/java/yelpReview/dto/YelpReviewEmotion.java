package yelpReview.dto;

import lombok.Data;

import java.util.Map;

@Data
public class YelpReviewEmotion {
	private String name;
	private String imageUrl;
	private String location;
	private Long rating;
	private String reviewContent;
	private Map<String, String> emotion;
}
