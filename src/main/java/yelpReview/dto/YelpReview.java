package yelpReview.dto;

import lombok.Data;

@Data
public class YelpReview {
	private String name;
	private String imageUrl;
	private String location;
	private Long rating;
	private String reviewContent;
}
