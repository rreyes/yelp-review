package yelpReview.dto;

import lombok.Data;

import java.util.List;

@Data
public class YelpInfoEmotion {

	private LocationInfo locationInfo;
	private List<YelpReviewEmotion> yelpReviews;
}
