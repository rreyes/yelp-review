package yelpReview.controllers;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.vision.CloudVisionTemplate;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.Feature.Type;

import yelpReview.services.DetectFaces;

@RestController
public class ImageController {
	
	@Autowired 
	private ResourceLoader resourceLoader;

	@Autowired 
	private CloudVisionTemplate cloudVisionTemplate;

	@GetMapping("/extractLabels")
	public Map<String, Float> extractLabels() {
		String imageUrl = "https://s3-media2.fl.yelpcdn.com/photo/3N031sJ46rxxZN6M7lr4Qg/o.jpg";
		
		AnnotateImageResponse response =
	    this.cloudVisionTemplate.analyzeImage(
	        this.resourceLoader.getResource(imageUrl), Type.LABEL_DETECTION);
	
		Map<String, Float> imageLabels =
				response
				.getLabelAnnotationsList()
				.stream()
				.collect(
					Collectors.toMap(
	                EntityAnnotation::getDescription,
	                EntityAnnotation::getScore,
	                (u, v) -> {
	                  throw new IllegalStateException(String.format("Duplicate key %s", u));
	            },
	            LinkedHashMap::new));
		
		return imageLabels;
	}
	
	  @GetMapping("/extractText")
	  public String extractText() {
		  String imageUrl = "https://s3-media2.fl.yelpcdn.com/photo/yDU3CEkw7IeaSvhVu8v3Nw/o.jpg";
		  
		  String textFromImage =
				  this.cloudVisionTemplate.extractTextFromImage(this.resourceLoader.getResource(imageUrl));
		  return "Text from image: " + textFromImage;
	  }
	  
	  @GetMapping("/detectFaces")
	  public Map<String, String> detectFaces() throws IOException {
		  String imageUrl = "https://s3-media3.fl.yelpcdn.com/photo/GQbhBOSv5RsGPUp-tis3Ow/o.jpg";

		  return DetectFaces.detectFaces(imageUrl);
	  }
}
